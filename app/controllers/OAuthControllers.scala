package controllers

import javax.inject._

import play.api.libs.json.Json
import play.api.mvc._
import services.users.OAuthToken._
import services.users.TokenService

@Singleton
class OAuthControllers @Inject()(tokenService: TokenService) extends Controller {

  def token(email: String, secret: String) = Action {
    tokenService.getToken(email, secret) match {
      case Some(token) => Ok (Json.toJson(token))
      case None => BadRequest(Json.obj("error" -> "Authentication failed"))
    }
  }
}
