package controllers.actions

import play.api.mvc.Security.AuthenticatedRequest
import play.api.mvc.{ActionBuilder, Request, Result}
import services.users.{BillingService, User}

import scala.concurrent.Future

trait Billable {

  def billingService: BillingService


  class BillingBuilder(recordAction: User => Unit)
    extends ActionBuilder[Request] {
    override def invokeBlock[A](request: Request[A], block: (Request[A]) => Future[Result]): Future[Result] = {
      request match {
        case authReq: AuthenticatedRequest[A, User] =>
          recordAction(authReq.user)
          block(authReq)
        case _ => throw new IllegalStateException("we could bill only auth users")
      }
    }
  }

  object BillingAction extends BillingBuilder(
    user => billingService.registerRequest(user)
  )

}
