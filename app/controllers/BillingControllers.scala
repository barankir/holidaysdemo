package controllers

import javax.inject._

import play.api.libs.json.Json
import play.api.mvc._
import services.users.BillingService

@Singleton
class BillingControllers @Inject()(billingService: BillingService) extends Controller {

  def report() = Action {implicit request =>
    val rep = billingService.report
    render {
      case Accepts.Html() => Ok(views.html.billing(rep))
      case Accepts.Json() => Ok(Json.obj("report" -> rep))
    }
  }

  def reportForUser(name: String) = Action {
    Ok(Json.obj("requests" -> billingService.report.getOrElse[Long](name, 0L)))
  }
}
