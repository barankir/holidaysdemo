package services.users

import play.api.libs.json.{Json, OWrites}

case class OAuthToken(token: String)

object OAuthToken {
  implicit val authTokenWrites: OWrites[OAuthToken] = Json.writes[OAuthToken]
}

