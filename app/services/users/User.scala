package services.users

import utils.Converters._

case class User(email: String) {
  def check(secret: String): Boolean = email == secret.decrypt
}
