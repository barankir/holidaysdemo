package services.users

import java.util.concurrent.atomic.AtomicLong
import javax.inject._

import scala.collection.concurrent.TrieMap


trait BillingService {

  def registerRequest(user: User)

  def report: Map[String, Long]

}

@Singleton
class InMemoryBillingService extends BillingService {

  private val requests = new TrieMap[String, AtomicLong]

  def registerRequest(user: User) {
    requests.putIfAbsent(user.email, new AtomicLong(0))
    requests(user.email).incrementAndGet()
  }

  def report: Map[String, Long] = requests.map(e => e._1 -> e._2.longValue).toMap[String, Long]

}
