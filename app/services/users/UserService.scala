package services.users

import javax.inject._

import services.EmailService


trait UserService {

  def login(email: String, secret: String): Option[User]

  def register(email: String, secret: String): Unit
}

@Singleton
class InMemoryUserService @Inject()(emailService: EmailService) extends UserService {

  override def login(email: String, secret: String): Option[User] = {
    Option(User(email)).filter(_.check(secret))
  }

  override def register(email: String, secret: String): Unit = sendEmail(email, secret)

  private def sendEmail(email: String, secret: String) = emailService.sendEmail(email, "Demo secret", s"Secret: $secret")

}
