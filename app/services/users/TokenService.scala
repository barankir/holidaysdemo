package services.users

import javax.inject._

import utils.CryptoUtil

import scala.collection.concurrent.TrieMap

trait TokenService {
  def findUser(token: String): Option[User]

  def getToken(email: String, secret: String): Option[OAuthToken]
}

@Singleton
class InMemoryTokenService @Inject()(userService: UserService) extends TokenService {

  private val tokens = new TrieMap[String, User]

  override def findUser(token: String): Option[User] = tokens.get(token)

  override def getToken(email: String, secret: String): Option[OAuthToken] = {
    userService.login(email, secret).map(user => {
      val token: String = CryptoUtil.generateToken(email)
      tokens.put(token, user)
      OAuthToken(token)
    })
  }
}