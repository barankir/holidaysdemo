package services.calendar

import java.time.LocalDate

case class DayOff(date: LocalDate, comment: String = "") extends Ordered[DayOff] {
  override def compare(that: DayOff): Int = date.compareTo(that.date)
}
