package services.calendar

import java.time.LocalDate

import scala.collection.immutable.TreeSet

class BusinessCalendarGenerator {

  def getCalendar(fromYear: Int, toYear: Int): TreeSet[DayOff] = {
    (for (i <- fromYear to toYear)
      yield getCalendar(i))
      .flatten.to[TreeSet]
  }

  def getCalendar(year: Int): Seq[DayOff] = {
    // http://mibius.com.ua/kalendar-2013/proizvodstvennyy-kalendar-na-2013-god-dlya-ukrainy.html
    val easter = calcEasterDay(year)
    Seq(DayOff(LocalDate.of(year, 1, 1), "новый год"),
      DayOff(LocalDate.of(year, 1, 1), "новый год"),
      DayOff(LocalDate.of(year, 1, 7), "Рождество"),
      DayOff(LocalDate.of(year, 3, 8), "8-е марта"),
      DayOff(LocalDate.of(year, 5, 1), "первомай"),
      DayOff(LocalDate.of(year, 5, 2), "первомай"),
      DayOff(LocalDate.of(year, 5, 9), "деньпобеды"),
      DayOff(LocalDate.of(year, 6, 28), "День конст"),
      DayOff(LocalDate.of(year, 8, 24), "День независ"),
      DayOff(easter, "Пасха"),
      DayOff(easter.plusDays(49), "Троица"))
  }

  private def calcEasterDay(year: Int) = {
    // актуально для year <= 2099 года
    // http://inf.1september.ru/view_article.php?ID=200901908
    val a = year % 19
    val b = year % 4
    val c = year % 7
    val d = (19 * a + 15) % 30
    val e = (2 * b + 4 * c + 6 * d + 6) % 7
    val f = d + e
    val oldStyleCorrection = 13
    if (f <= 9) LocalDate.of(year, 3, f + 22).plusDays(oldStyleCorrection)
    else LocalDate.of(year, 4, f - 9).plusDays(oldStyleCorrection)
  }
}
