package utils

import javax.inject._

import play.api._
import play.api.http.DefaultHttpErrorHandler
import play.api.libs.json._
import play.api.mvc.Results._
import play.api.mvc._
import play.api.routing.Router

import scala.concurrent._


@Singleton
class ErrorHandler @Inject()(
                              env: Environment,
                              config: Configuration,
                              sourceMapper: OptionalSourceMapper,
                              router: Provider[Router]
                            )
  extends DefaultHttpErrorHandler(env, config, sourceMapper, router) with RequestExtractors {

  implicit val errorWrites: OWrites[ErrorDto] = Json.writes[ErrorDto]

  override protected def onProdServerError(request: RequestHeader, e: UsefulException): Future[Result] = {
    request match {
      case Accepts.Json() => Future.successful(InternalServerError(Json.toJson(ErrorDto(e.id, e.description))))
      case _ => super.onProdServerError(request,e)
    }
  }

  override protected def onDevServerError(request: RequestHeader, e: UsefulException): Future[Result] = {
    Future.successful(InternalServerError(Json.toJson(ErrorDto(e.id, e.description, Option(e.toString)))))
  }

}