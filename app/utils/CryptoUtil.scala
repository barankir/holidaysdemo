package utils

import java.nio.charset.StandardCharsets
import java.security.MessageDigest
import java.util.UUID

import org.apache.commons.codec.binary.Base64
import play.api.libs.Crypto

object CryptoUtil {

  def encrypt(str: String): String = encode(Crypto.encryptAES(str))

  def decrypt(str: String): String = { Crypto.decryptAES(decode(str))}

  def encode(string: String): String = {
    Base64.encodeBase64String(string.getBytes("utf-8"))
  }

  def decode(string: String): String = {
    new String(Base64.decodeBase64(string), "utf-8")
  }

  def generateToken(name: String): String = {
    val key: String = UUID.randomUUID.toString
    var digest = MessageDigest.getInstance("SHA-256")
    val hash: Array[Byte] = digest.digest((name + key).getBytes(StandardCharsets.UTF_8))
    Base64.encodeBase64String(hash)
  }

}
