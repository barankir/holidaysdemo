package utils

case class ErrorDto(id: String, description: String, line: Option[String] = None)