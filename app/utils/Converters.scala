package utils

import java.time.LocalDate
import java.time.format.DateTimeFormatter.ISO_LOCAL_DATE

import services.calendar.DayOff

object Converters {

  implicit class StringImprovements(val str: String) {
    def toLocalDate: LocalDate = LocalDate.from(ISO_LOCAL_DATE.parse(str))
    def encrypt: String = CryptoUtil.encrypt(str)
    def decrypt: String = CryptoUtil.decrypt(str)
  }

  implicit class BoolToOption(val self: Boolean) extends AnyVal {
    def toOption[A](value: => A): Option[A] =
      if (self) Some(value) else None
  }

  implicit class LocalDateImprovements(val date: LocalDate) {
    def toDayOff: DayOff = DayOff(date)
  }

}
